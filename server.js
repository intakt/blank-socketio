var _, app, connection, express, fs, http, io, ip, port, url;

fs = require('fs');

ip = require('ip');

_ = require('lodash');

express = require('express');

app = express();

url = require('url');

http = require('http').Server(app);

io = require('socket.io')(http);

port = process.env.PORT || 8080;

app.use(express["static"](__dirname + "/"));

reload(app);

http.listen(port, function() {
  return console.log("-> :" + port);
});

connection = function(socket) {
  console.log(socket.id + " joined.");
  io.emit("joined", socket.id);
  socket.on('disconnect', function() {
    console.log(socket.id + " left.");
    return io.emit("left", socket.id);
  });
  return socket.on('leave', function(t) {
    return socket.disconnect();
  });
};

io.on('connection', connection);
