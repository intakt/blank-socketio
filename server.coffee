fs = require 'fs'
ip = require 'ip'
_ = require 'lodash'
express = require 'express'
app = express()
url = require 'url'
http = require('http').Server(app)
io = require('socket.io')(http)
port = process.env.PORT || 8080

app.use(express.static(__dirname + "/"))
reload(app)
http.listen(port, () -> console.log("-> :#{port}"))

connection = (socket) ->
  console.log socket.id + " joined."
  io.emit("joined", socket.id)
  socket.on('disconnect', () ->
    console.log socket.id + " left."
    io.emit("left", socket.id)
  )
  socket.on('leave', (t) -> socket.disconnect())

io.on('connection', connection )
