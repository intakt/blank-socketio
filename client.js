var socket;

socket = io();

console.log(socket);

socket.on("joined", function(r) {
  return console.log("user " + r + " joined.");
});

socket.on("left", function(r) {
  return console.log("user " + r + " left.");
});
